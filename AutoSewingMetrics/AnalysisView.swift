//
//  MatchView.swift
//  AutoSewingMetrics
//
//

import SwiftUI

struct AnalysisView: View {
    @StateObject var viewModel: ViewModel
    @State var analysisImage: UIImage?
    @State var result: BestMatchingReturnToSwift?
    @State var message: String = "Analyzing your sewn line..."
    


    var body: some View {
        VStack{
            Group{
                VStack {
                    if let analysisImage = analysisImage{
                        Image(uiImage: analysisImage)
                            .resizable()
                            .aspectRatio(contentMode: .fill)
                            .scaledToFit()
                            .cornerRadius(10)
                            .overlay(RoundedRectangle(cornerRadius: 10)
                                .stroke(Color.accentColor, lineWidth: 2))
                        .padding(EdgeInsets(top: 0, leading: 70, bottom: 40, trailing: 70))} 
                    else{Text("Please try with another photo.").foregroundColor(.red)}
                    Text(self.message)
                        .foregroundColor(.black)
                    ProgressView()
                        .progressViewStyle(CircularProgressViewStyle(tint: Color.gray))
                }
            }
        }
        .onAppear{
            DispatchQueue.global().async {
                // background task that finds the result from OpenCV wrapper
                
            if let analysisImage = self.analysisImage, let result = Wrapper.main(self.analysisImage)
                    {
                        
                        
                            
                            self.viewModel.analyzedImage = result.beforeMatch
                            self.viewModel.result = result.afterMatch
                            
                            
                            self.viewModel.overlap = result.overlap * 100
                            self.viewModel.distanceConsistency = result.distanceConsistency
                            
                            DispatchQueue.main.async {
                                self.viewModel.path.removeLast()
                                self.viewModel.path.append(NavigationDiff.targetResult(image: result.afterMatch))
                            }
                    } else {
                        DispatchQueue.main.async{
                            self.message = "Please try again with another photo."
                        }
                }
            }
            
           
        }

    }
}


//
//  DistortionView.swift
//  AutoSewingMetrics
//

import SwiftUI
import CoreImage

struct DistortionView: View {
    
    @State var editImage: UIImage?
    @State var originalOrientation: UIImage.Orientation?
    @State var undoImage: UIImage?
    @State var loading: Bool = false
    @State var showAlert: Bool = false
    @State private var animationAmount = 1.0
    @StateObject var viewModel: ViewModel
    
    var body: some View {
        VStack(alignment: .center, spacing: 20){
            VStack(alignment: .leading, spacing: 10){
                Text("Prepare your photo for analysis")
                    .bold()
                    .fontWeight(.bold)
                    .font(.title)
                Text("For analysis, we need to focus your image on one box from the manual.")
            }

            HStack
            {
                Button(action: {
                    if self.undoImage != nil {
                        self.editImage = self.undoImage
                    }
                }) {
                    Label("Undo", systemImage: "arrow.uturn.backward.circle")
                        .padding()
                        .foregroundColor(Color.white)
                }
                .background(Color.gray)
                .cornerRadius(10)
                
                Button(action: {
                    self.focusImageOnRectangle()
                }){
                    Label("Focus photo", systemImage: "camera.metering.center.weighted.average")
                        .foregroundColor(Color.white)
                        .padding()
                        .frame(maxWidth: .infinity)
                }
                .background(Color.black)
                .cornerRadius(10)
            
            }.padding(10)
            
            Spacer(minLength: 10)
            
            if self.loading {
                ProgressView()
                    .tint(.gray)
            }
            else {
                Image(uiImage: editImage!)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .scaledToFit()
                    .frame(maxHeight: .infinity)
                    .cornerRadius(10)
                    .overlay(RoundedRectangle(cornerRadius: 10)
                        .stroke(Color.accentColor, lineWidth: 2))
            }
            
            Spacer(minLength: 10)
            
            HStack{
                Spacer()
                    
                Button(action: {
                    self.accepted()
                }) {
                    Label("Analyze", systemImage: "rectangle.and.text.magnifyingglass")
                        .padding()
                        .foregroundColor(Color.white)
                }
                .background(Color.accentColor)
                .cornerRadius(10)
            }
        }
        .padding(EdgeInsets(top: 10, leading: 20, bottom: 0, trailing: 20))
        .alert(isPresented: $showAlert) {
            Alert(
                title: Text("Could not detect guide!"),
                message: Text("We could not detect the guide in the current photo.")
            )
        }
    }
   
    func accepted(){
        guard let editImage = self.editImage else { return }
        
        //let ciEdit = CIImage(image: self.editImage!)
        //let cgEdit = CIContext().createCGImage(ciEdit!, from: ciEdit!.extent)
        //self.editImage = UIImage(cgImage: cgEdit!, scale: self.editImage!.scale, orientation: self.originalOrientation!)
        
        self.viewModel.path.append(NavigationDiff.targetMatch(image: editImage))
        self.viewModel.analyzedImage = editImage
    }
    
    func focusImageOnRectangle(){
        self.loading = true
       
        DispatchQueue.global().async {
            let inputImage = CIImage(image: editImage!, options: [CIImageOption.applyOrientationProperty : true])
            let ciContext =  CIContext()
            let detector = CIDetector(ofType: CIDetectorTypeRectangle,
                                      context: ciContext,
                                      options: [CIDetectorAccuracy: CIDetectorAccuracyHigh])
            
            guard let rect = detector!.features(in: inputImage!).first as? CIRectangleFeature else {
                self.showAlert.toggle()
                self.loading = false
                return
            }
            
            // Create a dictionary with the correction data
            let correctionData: [String: Any] = [
                "inputTopLeft": CIVector(cgPoint: rect.topLeft),
                "inputTopRight": CIVector(cgPoint: rect.topRight),
                "inputBottomLeft": CIVector(cgPoint: rect.bottomLeft),
                "inputBottomRight": CIVector(cgPoint: rect.bottomRight)
            ]
            
            // Apply the perspective correction filter
            let perspectiveCorrection = inputImage?.applyingFilter("CIPerspectiveCorrection", parameters: correctionData)
            
            // Convert the result back to UIImage
            if let outputImage = perspectiveCorrection,
               let cgImage = CIContext().createCGImage(outputImage, from: outputImage.extent) {
                let correctedImage = UIImage(cgImage: cgImage, scale: 1.0, orientation: self.editImage!.imageOrientation)
                self.undoImage = self.editImage
                self.editImage = correctedImage
            }
        }
        
        self.loading = false
    }
}

/*
See the License.txt file for this sample’s licensing information.
*/

import SwiftUI
import PhotosUI

struct CameraView: View {
    @StateObject private var model = DataModel()
    @StateObject var viewModel: ViewModel
    @State private var showingImagePicker = false
    @State private var inputImage: UIImage?
    private static let barHeightFactor = 0.15
    @State var selectedItems: [PhotosPickerItem] = []
    
    @State private var selectedImage = UIImage()
    
    @State private var cameraTransitionAnimator: Bool = true
    
    var body: some View {
        GeometryReader { geometry in
            ViewfinderView(image:  $model.viewfinderImage )
                .overlay {
                    Color.black
                        .overlay {
                            ProgressView()
                                .controlSize(.extraLarge)
                        }
                        .opacity(cameraTransitionAnimator ? 1 : 0)
                    
                }
                .overlay(alignment: .top) {
                    Color.black
                        .opacity(0.75)
                        .frame(height: geometry.size.height * Self.barHeightFactor)
                }
                .overlay(alignment: .bottom) {
                    buttonsView()
                        .frame(height: geometry.size.height * Self.barHeightFactor)
                        .background(.black.opacity(0.75))
                }
                .overlay(alignment: .center)  {
                    Color.clear
                        .frame(height: geometry.size.height * (1 - (Self.barHeightFactor * 2)))
                        .accessibilityElement()
                        .accessibilityLabel("View Finder")
                        .accessibilityAddTraits([.isImage])
                }
                .background(.black)
        }
        .task {
            await model.camera.start()
            await model.loadPhotos()
            await model.loadThumbnail()
            
            Task.detached {
                try? await Task.sleep(nanoseconds: UInt64(1 * Double(NSEC_PER_SEC)))
                await MainActor.run {
                    withAnimation(.bouncy) {
                        cameraTransitionAnimator = false
                    }
                }
                
            }
            
        }
        .navigationBarTitleDisplayMode(.inline)
        //.navigationBarHidden(true)
        .ignoresSafeArea()
        .statusBar(hidden: true)
        /*.sheet(isPresented: $showingImagePicker, onDismiss: setImage) {
            ImagePicker(image: $inputImage)
                .onAppear {
                    model.camera.isPreviewPaused = true
                }
                .onDisappear {
                    model.camera.isPreviewPaused = false
                }
            }*/
    }
    
    private func buttonsView() -> some View {
        ZStack{
            HStack(alignment: .center) {
                Spacer()
                
                Button {
                    model.camera.takePhoto()
                } label: {
                    Label {
                        Text("Take Photo")
                    } icon: {
                        ZStack {
                            Circle()
                                .strokeBorder(.white, lineWidth: 3)
                                .frame(width: 62, height: 62)
                            Circle()
                                .fill(.white)
                                .frame(width: 50, height: 50)
                        }
                    }
                }
          
                Spacer()
            }
            
            HStack(alignment: .center, spacing: 60) {
                PhotosPicker(selection: $selectedItems, maxSelectionCount: 1, matching: .images){
                    Label {
                        Text("Gallery")
                    } icon: {
                        ThumbnailView(image: model.thumbnailImage)
                    }
                }
                
                Spacer()
            }
            
            
            
        }
        .buttonStyle(.plain)
        .labelStyle(.iconOnly)
        .padding()
        .onChange(of: selectedItems) {
            Task {
                if !selectedItems.isEmpty {
                    for item in selectedItems {
                        if let data = try? await item.loadTransferable(type: Data.self) {
                            if let uiImage = UIImage(data: data) {
                                self.selectedImage = uiImage
                            }
                        }
                    }
                    self.viewModel.path.append(NavigationDiff.targetDistortion(image: selectedImage))
                    
                    selectedItems.removeAll()
                }
            }
        }
    }
}

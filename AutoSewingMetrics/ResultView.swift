//
//  ResultView.swift
//  AutoSewingMetrics
//
//

import SwiftUI

struct ResultView: View {
    @StateObject var viewModel: ViewModel
    let columns = [GridItem(.flexible()), GridItem(.flexible())]
    
    var body: some View {
        VStack{
            VStack(alignment: .leading, spacing: 10){
                HStack{
                    Text("Your results are here!")
                        .bold()
                        .fontWeight(.bold)
                        .font(.system(size: 28))
                    Spacer()
                }
                Text("We matched your sewn line to the guide and found the following results:")
            }.padding(20)
            
            LazyVGrid(columns: columns){
                Image(uiImage: viewModel.analyzedImage!)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .scaledToFill()
                    .overlay(RoundedRectangle(cornerRadius: 10)
                        .stroke(Color.gray, lineWidth: 0.5))
                Image(uiImage: viewModel.result!)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .scaledToFill()
                    .overlay(RoundedRectangle(cornerRadius: 10)
                        .stroke(Color.accentColor, lineWidth: 5))
                Text("Your Photo")
                Text("Matched Lines")
                    .foregroundColor(Color.accentColor)
                    .bold()
            }.padding(EdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 20))
            Spacer()
            LazyVGrid(columns: columns, spacing: 15){
                ZStack {
                    Circle()
                        .foregroundColor(Color.accentColor) // You can change the color as needed
                        .frame(width: 100, height: 100) // Adjust the size of the circle
                    
                    Text(String(format: "%.1f%%", viewModel.overlap ?? 0.0))
                        .foregroundColor(Color.white)
                        .font(.system(size: 25))
                        .bold()

                }
                ZStack {
                    Circle()
                        .foregroundColor(Color.accentColor) // You can change the color as needed
                        .frame(width: 100, height: 100) // Adjust the size of the circle

                    Text(String(format: "%.1f%mm", viewModel.distanceConsistency ?? 0.0))
                        .foregroundColor(Color.white)
                        .font(.system(size: 25))
                        .bold()
                }
                Text("Replication Accuracy")
                    .foregroundColor(.gray)
                    .font(.system(size: 15))
                Text("Distance Variation")
                    .foregroundColor(.gray)
                    .font(.system(size: 15))
                
            }.padding(EdgeInsets(top: 0, leading: 10, bottom: 0, trailing: 10))
            
            Spacer()
        }
    }
}

//
//  AutoSewingMetricsApp.swift
//  AutoSewingMetrics
//
//

import SwiftUI

@main
struct AutoSewingMetricsApp: App {
    var body: some Scene {
        WindowGroup {
            StartView()
        }
    }
}

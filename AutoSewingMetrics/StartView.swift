//
//  StartView.swift
//  AutoSewingMetrics
//
//

import SwiftUI
import PDFKit
import PhotosUI

struct StartView: View {
    
    @State private var showingImagePicker = false
    
    @State var selectedItems: [PhotosPickerItem] = []
    @State private var selectedImage = UIImage()
    
    @StateObject private var viewModel = ViewModel()
 
    
    var body: some View {
        NavigationStack(path: self.$viewModel.path){
            VStack(spacing: 20){
                HStack{
                    Text("Welcome to")
                        .font(.system(size: 38))
                        +
                        Text(" SeamSmart!")
                            .italic()
                            .bold()
                            .foregroundColor(Color.accentColor)
                            .font(.system(size: 38))
                    Spacer()
                }
               
                HStack{
                    Text("To evaluate your sewing, print one of our guides, sew along the target line at a constant distance, and take a picture of your result.")
                    Spacer()
                }
                
                Spacer()
                
                Button {
                    self.setCamera()
                } label: {
                    Label("Take a photo", systemImage: "camera.fill")
                        .padding()
                        .foregroundColor(Color.white)
                        .frame(maxWidth: .infinity)
                }
                .buttonStyle(.borderedProminent)
                .background(Color.accentColor)
                .cornerRadius(10)
                
                PhotosPicker(selection: $selectedItems, maxSelectionCount: 1, matching: .images){
                    Label("Choose a photo", systemImage: "photo.fill")
                        .padding()
                        .foregroundColor(Color.white)
                        .frame(maxWidth: .infinity)
                }
                .buttonStyle(.borderedProminent)
                .background(Color.accentColor)
                .cornerRadius(10)
                
                Spacer()
                
                Button {
                    if let pdfPath = Bundle.main.path(forResource: "SewingWorksheet", ofType: "pdf")
                    {
                        let pdfURL = URL(fileURLWithPath: pdfPath)
                        let printerController = UIPrintInteractionController.shared
                            
                        URLSession.shared.dataTask(with: URLRequest(url: pdfURL)) { data, response, error in
                            printerController.printingItem = data
                            
                            DispatchQueue.main.async {
                                printerController.present(animated: true, completionHandler: nil)
                            }
                            
                        }.resume()
                    }
                } label: {
                    Label("Print the guide", systemImage: "printer.filled.and.paper")
                        .padding()
                        .foregroundColor(Color.white)
                        .frame(maxWidth: .infinity)
                }
                .background(Color.gray)
                .buttonStyle(.bordered)
                
                .cornerRadius(10)
                
                Spacer()
            }
            .padding(30)
            .navigationDestination(for: NavigationDiff.self) { diff in
                switch diff {
                case .targetCamera:
                    CameraView(viewModel: viewModel)
                case .targetDistortion(let image):
                    DistortionView(editImage: image, viewModel: viewModel)
                case .targetMatch(let image):
                    AnalysisView(viewModel: viewModel, analysisImage: image)
                case .targetResult(_):
                    ResultView(viewModel: viewModel)
                }
            }
    
            .onChange(of: selectedItems) {
                Task {
                    if !selectedItems.isEmpty {
                        for item in selectedItems {
                            if let data = try? await item.loadTransferable(type: Data.self) {
                                if let uiImage = UIImage(data: data) {
                                    self.selectedImage = uiImage
                                }
                            }
                        }
                        self.viewModel.path.append(NavigationDiff.targetDistortion(image: selectedImage))
                        selectedItems.removeAll()
                    }
                }
            }
        }
    }
    
    func setCamera() {
        self.viewModel.path.append(NavigationDiff.targetCamera)
    }
}

//
//  Wrapper.m
// 
//

#include <opencv2/imgproc.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#import <opencv2/opencv.hpp>
#import <opencv2/imgcodecs/ios.h>
#import "Wrapper.h"
#import <UIKit/UIKit.h>
#import <vector>
#include <iostream>
#include <cmath>
#include <array>
#include <iostream>
#include <vector>
#include <algorithm>
#include<tuple>
#import <Foundation/Foundation.h>

using namespace cv;
using namespace std;

@implementation BestMatchingInfo
@end

@implementation MatchingInfo
@end

@implementation BestMatchingReturnToSwift
@end

@implementation Wrapper : NSObject

// the minimum size of a component OpenCV returns to be included as a piece of line
int AREA_THRESHOLD = 20;

//the areas on the baseline which are checked for a matching
int Y_INTERVALSIZE_AROUND_CENTER = 20;
int STEP_SIZE_X_OFFSET = 40;

//he granulity of how fine the matching is calculated
int MATCHING_STEP_SIZE = 40;
int MATCHING_AREA_INTERVAL = 50;

//the tolerance for saying the pixels of the line are overlapping
int OVERLAP_THRESHOLD = 6;

//safety margin width for the edges of the sheet
int SAFETY_MARGIN_EDGE = 40;

//the baselines height in cm on the sheet
double BASELINE_LENGTH_IN_CM = 25.0;

//determines the line center
+ (CGPoint) getLineCenter:(NSMutableArray *)line{
    int max_row = -1;
    int min_row = INT_MAX;
                   
    for (int i = 0; i < line.count; i++) {
        CGPoint cgPoint = [[line objectAtIndex:i] CGPointValue];
        
        //x is row
        if (max_row < cgPoint.x){
            max_row = cgPoint.x;
        }
        
        if (min_row > cgPoint.x){
            min_row = cgPoint.x;
        }
    }

    int center_row =  round((min_row + max_row) / 2);
    
    for (int i = 0; i < line.count; i++) {
        CGPoint cgPoint = [[line objectAtIndex:i] CGPointValue];
        
        if (center_row == cgPoint.x) {
            return cgPoint;
        }
    }
    
    return CGPoint();
}

//determines the lines width by finding the min and max x pixel coordinates and subtracting them
+(int) getLineWidth:(NSMutableArray *)line{
    int max_column = -1;
    int min_column = INT_MAX;
    
    for (int i = 0; i < line.count; i++) {
        CGPoint cgPoint = [[line objectAtIndex:i] CGPointValue];
        
        //y is column
        if (max_column < cgPoint.y){
            max_column = cgPoint.y;
        }
        
        if (min_column > cgPoint.y){
            min_column = cgPoint.y;
        }
    }

    return (max_column - min_column);
}

//determine line height by finding the min and max y pixel coordinates and subtracting them
+(int) getLineHeight:(NSMutableArray *)line{
    int max_row = -1;
    int min_row = INT_MAX;
    
    for (int i = 0; i < line.count; i++) {
        CGPoint cgPoint = [[line objectAtIndex:i] CGPointValue];
        
        //y is column
        if (max_row < cgPoint.x){
            max_row = cgPoint.x;
        }
        
        if (min_row > cgPoint.x){
            min_row = cgPoint.x;
        }
    }
    
    return (max_row - min_row);
}

//remove artifacts of the image that could be classified as potential parts of a line
+ (Mat)cleanImageMask:(Mat)image_mask {
    
    Mat labels, stats, centroids;
    
    int output = connectedComponentsWithStats(image_mask, labels, stats, centroids, 8);
        
    //https://docs.opencv.org/3.4/d1/d1b/group__core__hal__interface.html
    //labels type is CV_32S (32 bit signed integer)
    //stats type is CV_32S (32 bit signed integer)
    //https://docs.opencv.org/3.4/d2/de8/group__core__array.html#ga48af0ab51e36436c5d04340e036ce981
    //image_mask type is CV_8U (8bit unsigned integer)
    
    int rows = image_mask.rows;
    int columns = image_mask.cols;
    
    //discard recognized components which are smaller than the given threshold
    for (int i = 0; i < rows; i++) {
        for (int j = 0;  j < columns; j++) {
            
            int32_t label = labels.at<int32_t>(i, j);
            int32_t area = stats.at<int32_t>(label, cv::CC_STAT_AREA);
            
            if (area < AREA_THRESHOLD) {
                // mark this pixel as background
                
                uint8_t &pixel = image_mask.at<uint8_t>(i,j);
                pixel = 0;
            }
        }
    }
    
    //in DistortionView, we perspective correct onto the sheet and the edges of the sheet sometimes get picked up as colors, messing with the algorithm
    //we go along all the edges of the picture and label them as background
    for (int i = 0; i < rows; i++) {
        for (int j = 0;  j < SAFETY_MARGIN_EDGE; j++) {
            uint8_t &pixel_min_x = image_mask.at<uint8_t>(i,j);
            pixel_min_x = 0;
            uint8_t &pixel_max_x = image_mask.at<uint8_t>(i,(image_mask.cols-1) - j);
            pixel_max_x = 0;
        }
    }
    for (int j = 0; j < columns; j++) {
        for (int i = 0;  i < SAFETY_MARGIN_EDGE; i++) {
            uint8_t &pixel_min_y = image_mask.at<uint8_t>(i,j);
            pixel_min_y = 0;
            uint8_t &pixel_max_y = image_mask.at<uint8_t>((image_mask.rows-1) - i,j);
            pixel_max_y = 0;
        }
    }

    return image_mask;
}

//returns the pixel associated with a line of the provided image mask
+ (NSMutableArray*)extractLinePixels:(Mat)image_mask {
    NSMutableArray *linePixels = [NSMutableArray array];
    
    int rows = image_mask.rows;
    int columns = image_mask.cols;
 
    
    for (int i = 0; i < rows; ++i) {
        for (int j = 0;  j < columns; ++j) {
            uint8_t gray_color = image_mask.at<uint8_t>(i, j);
            if (gray_color == 255) {
                NSValue *point = [NSValue valueWithCGPoint:CGPointMake(i, j)];
                
                CGPoint cgPoint = [point CGPointValue];
            
                [linePixels addObject:point];
                break;
            }
        }
    }
    
    return linePixels;
}

//returns the cm according to the used ratio
double pixelToCm(double x, double pixel_per_cm) {
    return x / pixel_per_cm;
}

//calculate the standard deviation in a provided array
+ (double) calculate_standard_deviation:(NSMutableArray *)data{
    
    int n = data.count;

    if (n < 2){
        return 0.0;
    }
    
    //Step 1: Calculate the mean (average) of the data
    double sum = 0.0;
    for (NSNumber *number in data) {
        sum += [number doubleValue];
    }
    double mean = sum / n;
    
    //Step 2: Calculate the squared differences from the mean
    NSMutableArray *squaredDiff = [NSMutableArray arrayWithCapacity:data.count];
    for (NSNumber *number in data) {
        double x = [number doubleValue];
        double diff = x - mean;
        double squared = pow(diff, 2);
        [squaredDiff addObject:@(squared)];
    }
        
    //Step 3: Calculate the variance as the average of squared differences
    double sum_squaredDiff = 0.0;
    for (NSNumber *number in squaredDiff) {
        sum_squaredDiff += [number doubleValue];
    }
    double variance = sum_squaredDiff / n;

    //Step 4: Calculate the standard deviation as the square root of variance
    double std_deviation = pow(variance, 0.5);
        
    return std_deviation;
}

//calculate the distance consistency for evaluating the sewn line
+ (double)calculateDistanceConsistency:(NSMutableArray *)base_line matching:(NSMutableArray *)matching{
    
    int height_in_pixel = [self getLineHeight:base_line];
    double pixel_per_cm = height_in_pixel / BASELINE_LENGTH_IN_CM;
        
    NSMutableArray *distances_in_px = [NSMutableArray array];
    NSMutableArray *matched_base_points = [matching objectAtIndex:0];
    NSMutableArray *matched_sewn_points = [matching objectAtIndex:1];
    
    for (int i = 0; i < matched_base_points.count; i++) {
        CGPoint sewn_point = [[matched_sewn_points objectAtIndex:i] CGPointValue];
        CGPoint base_point = [[matched_base_points objectAtIndex:i] CGPointValue];
        
        double distance = std::sqrt(std::pow(base_point.x - sewn_point.x, 2) + std::pow(base_point.y - sewn_point.y, 2));
        
        [distances_in_px addObject:@(distance)];
    }
        
    NSMutableArray *distances_in_cm = [NSMutableArray array];
    
    for (int i = 0; i < distances_in_px.count; i++) {
        double distanceInPx = [[distances_in_px objectAtIndex:i] doubleValue];
        [distances_in_cm addObject:@(pixelToCm(distanceInPx, pixel_per_cm))];
    }
    
    double std_deviation_in_cm = [self calculate_standard_deviation:distances_in_cm];
    
    double std_deviation_in_mm = std_deviation_in_cm * 10;

    return std_deviation_in_mm;
}

//Overlap the two lines from an interval of test points around the center of the green base line
//also move along x-axis in steps
+ (BestMatchingInfo *) determineBestMatching:(NSMutableArray *)base_line sewnLine:(NSMutableArray *)sewn_line{
    
    CGPoint base_line_center = [self getLineCenter:base_line];
    
    CGPoint sewn_line_center = [self getLineCenter:sewn_line];
    int sewn_line_width = [self getLineWidth: sewn_line];
    
    CGPoint best_offset = CGPointMake(0, 0);
    double best_overlap = 0.0;
    NSMutableArray *best_matching = [NSMutableArray array];
    
    //determine the points on the base line used for matching
    NSMutableArray *base_line_test_points = [NSMutableArray array];
    
    for (int i = 0; i < base_line.count; i++) {
        CGPoint cgPoint = [[base_line objectAtIndex:i] CGPointValue];
        NSValue *pointValue = [NSValue valueWithCGPoint:cgPoint];
        
        if ((base_line_center.x - Y_INTERVALSIZE_AROUND_CENTER) <=  cgPoint.x && cgPoint.x <= (base_line_center.x + Y_INTERVALSIZE_AROUND_CENTER)) {
            [base_line_test_points addObject: pointValue];
        }
    }
    
    
    for (int i = 0; i < base_line_test_points.count; i++) {
        
        CGPoint test_point = [[base_line_test_points objectAtIndex:i] CGPointValue];
        
        for (int x_offset = (0-sewn_line_width); x_offset <= sewn_line_width; x_offset++)
        {
            
            if (x_offset % STEP_SIZE_X_OFFSET == 0){
                CGPoint offset = CGPointMake(test_point.x - sewn_line_center.x, test_point.y - sewn_line_center.y + x_offset);
                
               
                
                //determine the matching and quality of matching
                MatchingInfo *tuple = [self findMatching:base_line sewnLine:sewn_line offset:offset];
                
                if (best_overlap < tuple.overlap){
                    best_offset = offset;
                    best_overlap = tuple.overlap;
                    best_matching = tuple.matching;
                }
                
                if (x_offset > 0 && tuple.overlap == 0.0){
                    break;
                }
            }
        }
        
        if (best_overlap == 1.0) {
            break;
        }
    }
    
    BestMatchingInfo *result = [[BestMatchingInfo alloc] init];
    
    result.matching = best_matching;
    result.overlap = best_overlap;
    result.offset = best_offset;
    
    return result;
}


//helper method to determine a matching: pixel to matched pixel from line to line
+ (MatchingInfo *) findMatching:(NSMutableArray *)base_line sewnLine:(NSMutableArray *)sewn_line offset:(CGPoint)offset{
    
    int overlap_count = 0;
    int matching_index = 0;
    
    NSMutableArray *matched_base_points = [NSMutableArray array];
    NSMutableArray *matched_sewn_points = [NSMutableArray array];
    
    for (int i = 0; i < sewn_line.count; i++) {
        if (i % MATCHING_STEP_SIZE == 0){
            CGPoint sewn_point = [[sewn_line objectAtIndex:i] CGPointValue];
            NSValue *sewn_point_value = [NSValue valueWithCGPoint:sewn_point];
            [matched_sewn_points addObject:sewn_point_value];
            
            int best_distance = INT_MAX;
            
            CGPoint transformed_sewn_point = CGPointMake(sewn_point.x + offset.x, sewn_point.y + offset.y);
            
            for (int j = 0; j < base_line.count; j++) {
                CGPoint base_point = [[base_line objectAtIndex:j] CGPointValue];
                
                if ( (transformed_sewn_point.x - MATCHING_AREA_INTERVAL) <= base_point.x && base_point.x <= (transformed_sewn_point.x + MATCHING_AREA_INTERVAL))
                {
                    double distance = std::sqrt(std::pow(base_point.x - transformed_sewn_point.x, 2) + std::pow(base_point.y - transformed_sewn_point.y, 2));
                    
                    if (distance < best_distance){
                        best_distance = distance;
                        NSValue *base_point_value = [NSValue valueWithCGPoint:base_point];
                        
                        if (matched_base_points.count < matching_index + 1){
                            [matched_base_points addObject:base_point_value];
                        }
                        
                        else {
                            matched_base_points[matching_index] = base_point_value;
                        }
                    }
                }
            }
            
            if (best_distance < OVERLAP_THRESHOLD){
                overlap_count++;
            }
            
            matching_index++;
        }
    }
    
    NSMutableArray *matching = [NSMutableArray array];
    [matching addObject:matched_base_points];
    [matching addObject:matched_sewn_points];
    
    double quality_of_matching = overlap_count / double (matching_index);
    
    MatchingInfo *result = [[MatchingInfo alloc] init];
    
    result.matching = matching;
    result.overlap = quality_of_matching;
    
    return result;
}

//helper function to rotate an image by a given angle in degrees
+ (Mat)rotateImage:(Mat)inputImage rotationAngle:(double)rotationAngle{
    // Calculate the center of the original image
    Point2f original_center((inputImage.cols - 1) / 2.0, (inputImage.rows - 1) / 2.0);

    // Define the new size
    Size_<int> new_size(inputImage.rows, inputImage.cols);

    // Calculate the center of the new size
    Point2f new_center(new_size.width / 2.0, new_size.height / 2.0);

    // Get the rotation matrix
    Mat rotation_matrix = getRotationMatrix2D(original_center, rotationAngle, 1.0);

    // Combine the rotation and translation matrices
    Mat transformation_matrix = rotation_matrix.clone();
    transformation_matrix.at<double>(0, 2) += new_center.x - original_center.x;
    transformation_matrix.at<double>(1, 2) += new_center.y - original_center.y;
    
    warpAffine(inputImage, inputImage, transformation_matrix, new_size);
    
    return inputImage;
}


//the main method of the algorithm
+ (BestMatchingReturnToSwift *)main:(UIImage *)image {
    @try{
    Mat inputImage;
    //leaves as bgr
    UIImageToMat(image, inputImage);
   
    switch (image.imageOrientation) {
        case 0:
            break;
            
        case 1:
            inputImage = [self rotateImage:inputImage rotationAngle:180];
            break;
            
        case 2:
            inputImage = [self rotateImage:inputImage rotationAngle:90];
            break;
        
        case 3:
            inputImage = [self rotateImage:inputImage rotationAngle:270];
            break;
            
        default:
            break;
    }

    cvtColor(inputImage, inputImage, cv::COLOR_BGR2RGB);
    cvtColor(inputImage, inputImage, cv::COLOR_RGB2HSV);
    
    //OpenCVs olor integration into iOS is broken
    //As OpenCV uses BGR rather than RGB, on the HSV spectrum, red an blue are swapped on iOS
    //To select red, we need to select blue
    Mat green_image_mask;
    inRange(inputImage, Scalar(30, 30, 30), Scalar(80, 255, 255), green_image_mask);
    
    Mat red_image_mask;
    inRange(inputImage, Scalar(110,50,50), Scalar(130,255,255), red_image_mask);
    
    red_image_mask = [self cleanImageMask:red_image_mask];
    green_image_mask = [self cleanImageMask:green_image_mask];
    
    NSMutableArray *base_line = [NSMutableArray array];
    NSMutableArray *sewn_line = [NSMutableArray array];
    
    base_line = [self extractLinePixels:green_image_mask];
    sewn_line = [self extractLinePixels:red_image_mask];
    
     if (base_line.count == 0 || sewn_line.count == 0) {
        NSLog(@"Error in line detection");
        return nil;
    }
        
        
    BestMatchingInfo *result = [self determineBestMatching:base_line sewnLine:sewn_line];
    if (!result) {
        NSLog(@"Error in matching");
        return nil;
    }
    
    //Create images of the matching for before and after moving the sewn line onto the base line
    Mat after_matching_image(red_image_mask.size(), CV_8UC3, Scalar(255,255,255));
    Mat before_matching_image(red_image_mask.size(), CV_8UC3, Scalar(255,255,255));

    int rows = after_matching_image.rows;
    int columns = after_matching_image.cols;
    
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++){
            if (green_image_mask.at<uint8_t>(i, j) == 255){
                Vec3b &pixel_after = after_matching_image.at<Vec3b>(i, j);
                pixel_after = Vec3b(0,255,0);
                Vec3b &pixel_before = before_matching_image.at<Vec3b>(i, j);
                pixel_before = Vec3b(0,255,0);
            }
        }
    }
    
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++){
            if (red_image_mask.at<uint8_t>(i, j) == 255){
                Vec3b &pixel_after = after_matching_image.at<Vec3b>(i + result.offset.x, j + result.offset.y);
                pixel_after = Vec3b(255,0,0);
                Vec3b &pixel_before = before_matching_image.at<Vec3b>(i, j);
                pixel_before = Vec3b(255,0,0);
            }
        }
    }
    
    //return from wrapper to the UI
    BestMatchingReturnToSwift *matchingReturnToSwift = [[BestMatchingReturnToSwift alloc] init];
    
    matchingReturnToSwift.beforeMatch = MatToUIImage(before_matching_image);
    matchingReturnToSwift.afterMatch = MatToUIImage(after_matching_image);
    matchingReturnToSwift.overlap = result.overlap;
    matchingReturnToSwift.distanceConsistency = [self calculateDistanceConsistency:base_line matching:result.matching];
    
    return matchingReturnToSwift;
    } @catch (NSException *exception){
        NSLog(exception.reason);
        return nil;
    }
}

@end

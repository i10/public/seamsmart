//
//  NavigationDifferantiator.swift
//  AutoSewingMetrics
//
//

import Foundation
import UIKit

enum NavigationDiff : Hashable {
    case targetCamera
    case targetDistortion(image: UIImage)
    case targetMatch(image: UIImage)
    case targetResult(image: UIImage)
}

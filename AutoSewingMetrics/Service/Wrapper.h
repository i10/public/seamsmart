//
//  Wrapper.h
//  test4
//
//

#ifndef Wrapper_hpp
#define Wrapper_hpp
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface BestMatchingInfo : NSObject
@property (nonatomic) NSMutableArray *matching;
@property (nonatomic) double overlap;
@property (nonatomic) CGPoint offset;
@end

@interface MatchingInfo : NSObject
@property (nonatomic) NSMutableArray *matching;
@property (nonatomic) double overlap;
@end


@interface BestMatchingReturnToSwift : NSObject
@property (nonatomic) UIImage* beforeMatch;
@property (nonatomic) UIImage* afterMatch;
@property (nonatomic) double overlap;
@property (nonatomic) double distanceConsistency;
@end


@interface Wrapper: NSObject

+ (BestMatchingReturnToSwift *)main:(UIImage *)image;

@end

#endif /* Wrapper_h */

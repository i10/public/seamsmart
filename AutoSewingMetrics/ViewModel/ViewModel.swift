//
//  ViewModel.swift
//  AutoSewingMetrics
//

import Foundation
import SwiftUI
import PDFKit
import Combine

class ViewModel: NSObject, ObservableObject {
    @Published var path = NavigationPath()
    
    public var pdfWorkingSheet: URL?
    public var analyzedImage: UIImage?
    public var result: UIImage?
    public var overlap: Double?
    public var distanceConsistency: Double?
    private var imageTakenCancellable: AnyCancellable?
    
    
    override init(){
        super.init()
        self.imageTakenCancellable = NotificationCenter.default.publisher(for: Notification.Name("de.rwth-aachen.hci.seam-smart.dictatePhoto"))
            .receive(on: DispatchQueue.main)
            .compactMap({ $0.object as? Data })
            .sink(receiveValue: { [weak self] imageData in
                if let image = UIImage(data: imageData) {
                    self?.path.append(NavigationDiff.targetDistortion(image: image))
                }
                
            })
        
    }
    
    init(analyzedImage: UIImage, result: UIImage){
        self.analyzedImage = analyzedImage
        self.result = result
    }
}

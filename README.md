# SeamSmart
SeamSmart is an iOS application that can support sewing training by presenting metrics providing feedback on how well a user can sew along a given line at a constant distance.

SeamSmart is a supplementary iOS application to the research publication [Lahaye et al., How’s Your Sewing? Investigating Metrics to Automatically Assess Sewing Expertise, CHI EA '24](https://doi.org/10.1145/3613905.3651067).

Further information can be found at the [project website](https://hci.rwth-aachen.de/sewing-assessment).

## Workflow
![SeamSmart app workflow](https://hci.rwth-aachen.de/public/SeamSmart/workflow.jpg)

a) Sew along the line on one of the provided printed worksheets.
b) Take a photo of the result using the SeamSmart app.
c) View the calculated feedback assessment metrics.

## Installation
1. Open the project in Xcode
2. Select a _development team_ in the Signing & Capabilities editor
3. Download the [OpenCV framework](https://opencv.org/releases/) (iOS pack) and place the .framework file in the AutoSewingMetrics/Frameworks folder. The app was developed using the OpenCV version 4.8.0

## Collaboration
When submitting commits, please edit the following parameters in project.pbxproj:
1. Clear `DEVELOPMENT_TEAM`
2. Set the parameter `PRODUCT_BUNDLE_IDENTIFIER` back to _temporaryIdentifier.pleaseRemove_

You can do so by clearing the _development team_ and setting the _bundle identifier_ in the Signing & Capabilities editor. Otherwise, you will make your developer ID public.

Any collaboration is welcome. To continue this research project with us, please message Marcel Lahaye at lahaye@cs.rwth-aachen.de.

## Authors
The SeamSmart project is implemented by:
- Marcel Lahaye
- Ricarda Rahm
- Andreas Dymek
- Adrian Wagner
- Judith Ernstberger
- Filiz Günal
- Kevin Fiedler
- Jan Borchers

## License
The SeamSmart © 2024 project by Marcel Lahaye, Ricarda Rahm, Andreas Dymek, Adrian Wagner, Judith Ernstberger, and Jan Borchers is licensed under CC BY-SA 4.0
https://creativecommons.org/licenses/by-sa/4.0/

The linked OpenCV library is licensed under the Apache 2 License.
https://opencv.org/license/
